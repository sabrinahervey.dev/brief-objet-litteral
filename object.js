//déclaration de l'objet et ses propriétés
let movie = {
    titleMovie: "The Lord of the ring",
    year: 2001,
    director: "Peter Jackson",
    actor: ["Elijah Wood" , "Ian McKellen"],
    favoriteMovie: true,
    movieRating: "4.8",

    //ajout de la méthode setDuration qui va calculer la durée du film en minute
    setDuration: function(hours, minutes) {
        this.duration = hours * 60 + minutes;
    }
}
//ajout de la propriété duration initialisé à 0
movie.duration = 0;
//déclaration durée du film
movie.setDuration(2, 58);

//affichage de la durée (178)
console.log(movie.duration);
console.log(movie.year);
console.log(movie.actor);

